﻿## Описание задания

Задача №2: "Война в долине теней".

Карточка Squad должна содержать следующие данные:

* фракция (перечисляемый тип),
* Сила удара
* Защита
* Здоровье
* Ловкость
* Уклонение
* Тип боя (ближний, дальний)


## Цели работы

На примере программы на языке С++:

* изучить объекты с разными видами состояний;
* определить инвариант класса;
* освоить работу с обобщёнными контейнерами стандартной библиотеки С++
* освоить работу с потоками ввода / вывода стандартной библиотеки С++
* освоить использование основных паттернов объектно-ориентированного проектирования

## Инвариант класса Squad

```cpp
    bool invariant() {
		bool result= (0 <= this->strength && this->strength <= 100) && (0 <= this->dexterity && this->dexterity <= 100) && (0 <= this->defence && this->defence <= 100) &&(0 <= this->dodging && this->dodging <= 100) && (0 <= this->health && this->health <= 100);
		return result;
	};
```

## Выводы
