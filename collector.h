#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#include <memory>
#include <sstream>
#include <fstream>

template<typename T>
T readItem(std::istream& is, bool & flag)
{
	T result;
	if (is.read((char *)(&result), sizeof(result))) {
		flag = !is.eof();
		return result;
	}
	else {
		flag = false;
		return result;
	}
}


class ICollectable
{
public:
	virtual ~ICollectable() = default;
	virtual void print() = 0;
	virtual bool write(std::ostream& os) = 0;
};

class ACollector
{
	std::vector<std::shared_ptr<ICollectable>> _items;
	std::vector<bool>                          _removed_signs;
	size_t                                     _removed_count = 0;

	bool    invariant() const
	{
		return _items.size() == _removed_signs.size() && _removed_count <= _items.size();
	}
	
public:
	virtual ~ACollector() = default;

	virtual std::shared_ptr<ICollectable> read(std::istream& is, bool & flag) = 0;

	size_t getSize() const { return _items.size(); }

	std::shared_ptr<ICollectable> getItem(size_t index) const
	{
		assert(index < _items.size());

		return _items[index];
	}

	bool isRemoved(size_t index) const
	{
		assert(index < _removed_signs.size());

		return _removed_signs[index];
	}

	void addItem(std::shared_ptr<ICollectable> item)
	{
		_items.emplace_back(item);
		_removed_signs.emplace_back(false);
	}

	void removeItem(size_t index)
	{
		assert(index < _items.size());
		assert(index < _removed_signs.size());

		if (!_removed_signs[index])
		{
			_removed_signs[index] = true;
			_removed_count++;
		}
	}

	void updateItem(size_t index, const std::shared_ptr<ICollectable> & item)
	{
		assert(index < _items.size());

		_items[index] = item;
	}

	void clean()
	{
		_items.clear();
		_removed_signs.clear();
		_removed_count = 0;
	}

	void printCollection() {
		for (size_t i = 0; i < _items.size(); i++)
		{
			if (!_removed_signs[i]) {
				_items[i]->print();
			}	
		}
	}

	bool loadCollection(const std::string file_name)
	{
		std::ifstream fin;
		fin.open(file_name);
		if (fin.is_open())
		{
			std::cout << "The file is open" << std::endl;
			bool flag = true;
			while (flag) {
				auto temp = read(fin, flag);
				if (flag)
				{
					addItem(temp);
				}
			}
			fin.close();
			assert(invariant());
			return true;
		}
		else {
			std::cout << "The file is not open" << std::endl;
			return false;
		}
	}

	bool saveCollection(const std::string file_name) const
	{
		assert(invariant());
		std::ofstream fout;
		fout.open(file_name);
		if (fout.is_open())
		{
			std::cout << "The file is open" << std::endl;
			for (size_t i = 0; i < _items.size(); ++i)
			{
				if (!_removed_signs[i])
					_items[i]->write(fout);

			}
			fout.close();
		}
		else {
			std::cout << "The file is not open" << std::endl;
		}
		return fout.good();
		
	}

};
