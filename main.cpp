#include <iostream>
#include <string>
#include <cassert>

#include <sstream>

#include "collector.h"


using namespace std;

class Squad:public ICollectable {
public:
	enum Fraction {
		MAGISTER,
		PALADIN,
		GODWOKEN

	};
	enum FightType {
		MELEE,
		RANGED
	};
	Fraction fraction;
	int strength;
	int defence;
	int health;
	int dexterity;
	int dodging;
	FightType fightType;

	bool invariant() {
		bool result= (0 <= this->strength && this->strength <= 100) && (0 <= this->dexterity && this->dexterity <= 100) && (0 <= this->defence && this->defence <= 100) &&(0 <= this->dodging && this->dodging <= 100) && (0 <= this->health && this->health <= 100);
		return result;
	};
	
	Squad(Squad::Fraction f, int strength, int defence, int health, int dexterity, int dodging, Squad::FightType ft) {
		this->fraction = f;
		this->strength = strength;
		this->defence = defence;
		this->health = health;
		this->dexterity = dexterity;
		this->dodging = dodging;
		this->fightType = ft;
		assert(invariant());
	};

	Squad(int f, int strength, int defence, int health, int dexterity, int dodging, int ft) {
		Fraction frac;
		switch (f)
		{
		case 0: frac = Fraction::MAGISTER;
			break;
		case 1: frac = Fraction::PALADIN;
			break;
		case 2: frac = Fraction::GODWOKEN;
			break;
		default:
			frac = Fraction::GODWOKEN;
			break;
		}
		this->fraction = frac;
		this->strength = strength;
		this->defence = defence;
		this->health = health;
		this->dexterity = dexterity;
		this->dodging = dodging;
		FightType Ft;
		switch (ft)
		{
		case 0: Ft = FightType::MELEE;
			break;
		case 1: Ft = FightType::RANGED;
			break;
		default:  Ft = FightType::MELEE;
			break;
		}
		this->fightType = Ft;
		assert(invariant());
	};
	Squad() {

	};

	virtual bool write(std::ostream& os) override {
		Squad temp(this->fraction, this->strength, this->defence, this->health, this->dexterity, this->dodging, this->fightType);
		os.write((char*)&temp, sizeof(Squad));
		return true;
	};

	void print() override{
		cout << "-------------------------------------------------------------------" << endl;
		switch (this->fraction)
		{
		case Squad::Fraction::GODWOKEN:
			cout << "Fraction: Godwoken" << endl;
			break;
		case Squad::Fraction::MAGISTER:
			cout << "Fraction: Magister" << endl;
			break;
		case Squad::Fraction::PALADIN:
			cout << "Fraction: Paladin" << endl;
			break;
		}

		cout << "Strength: " << this->strength << endl << "Defence: " << this->defence << endl <<
			"Haelth: " << this->health << endl << "Dexterity: " << this->dexterity << endl << "Dodging: " << this->dodging << endl;
		if (this->fightType == Squad::FightType::MELEE) {
			cout << "Fight Type: Melee" << endl;
		}
		else {
			cout << "Fight Type: Ranged" << endl;
		}
		cout << "-------------------------------------------------------------------" << endl;
	};

    ~Squad() {};
};

class ItemCollector : public ACollector
{
public:

	virtual shared_ptr<ICollectable> read(istream& is, bool & flag) override
	{
		Squad result = readItem<Squad>(is, flag);
		return make_shared<Squad>(result);
	}
};

bool performCommand(const vector<string> & args, ItemCollector & col)
{
	if (args.empty())
		return false;
	
	if (args[0] == "l" || args[0] == "load")
	{
		string filename = (args.size() == 1) ? "hw.data" : args[1];

		if (!col.loadCollection(filename))
		{
			cerr << "Îøèáêà ïðè çàãðóçêå ôàéëà '" << filename << "'" << endl;
			return false;
		}

		return true;
	}

	if (args[0] == "s" || args[0] == "save")
	{
		string filename = (args.size() == 1) ? "hw.data" : args[1];

		if (!col.saveCollection(filename))
		{
			cerr << "Îøèáêà ïðè ñîõðàíåíèè ôàéëà '" << filename << "'" << endl;
			return false;
		}

		return true;
	}

	if (args[0] == "c" || args[0] == "clean")
	{
		if (args.size() != 1)
		{
			cerr << "Íåêîððåêòíîå êîëè÷åñòâî àðãóìåíòîâ êîìàíäû clean" << endl;
			return false;
		}

		col.clean();

		return true;
	}

	if (args[0] == "r" || args[0] == "remove")
	{
		if (args.size() != 2)
		{
			cerr << "Íåêîððåêòíîå êîëè÷åñòâî àðãóìåíòîâ êîìàíäû remove" << endl;
			return false;
		}

		col.removeItem(stoul(args[1]));
		return true;
	}

	if (args[0] == "v" || args[0] == "view")
	{
		if (args.size() != 1)
		{
			cerr << "Íåêîððåêòíîå êîëè÷åñòâî àðãóìåíòîâ êîìàíäû view" << endl;
			return false;
		}

		col.printCollection();
		return true;
	}

	if (args[0] == "a" || args[0] == "add")
	{
		if (args.size() != 8)
		{
			cerr << "Íåêîððåêòíîå êîëè÷åñòâî àðãóìåíòîâ êîìàíäû add" << endl;
			return false;
		}

		col.addItem(make_shared<Squad>(stoul(args[1]), stoul(args[2]), stoul(args[3]), stoul(args[4]), stoul(args[5]), stoul(args[6]), stoul(args[7])));
		return true;
	}

	if (args[0] == "u" || args[0] == "update")
	{
		if (args.size() != 9)
		{
			cerr << "Íåêîððåêòíîå êîëè÷åñòâî àðãóìåíòîâ êîìàíäû update" << endl;
			return false;
		}

		col.updateItem(stoul(args[1]), make_shared<Squad>(stoul(args[2]), stoul(args[3]), stoul(args[4]), stoul(args[5]), stoul(args[6]), stoul(args[7]), stoul(args[8])));
		return true;
	}


	cerr << "Íåäîïóñòèìàÿ êîìàíäà '" << args[0] << "'" << endl;
	return false;
}


int main(int argc, char* argv[]) {
	setlocale(LC_ALL, "Rus");// ô-öèÿ äëÿ âûâîäà êèðèëëèöû
	ItemCollector ic;
	if (argc <= 1) {
		string str = "";
		cout << "input a command" << endl;
		getline(cin, str);
		while (str != "")
		{
			int j = 1;
			vector<string> args;
			args.push_back(str);
			while (str != "") {
				cout << "input " << j << "th" << " argument" << endl;
				getline(cin, str);
				j++;
				if (str != "")
				{
					args.push_back(str);
				}
			}
			performCommand(args, ic);
			cout << "input a command" << endl;
			getline(cin, str);
		}
	} else {
		vector<string> args;
		for (size_t i = 1; i < argc; i++)
		{
			string str = argv[i];
			args.push_back(str);
		}
		performCommand(args, ic);
		args.clear();
		string str = "v";
		args.push_back(str);
		performCommand(args, ic);
	}
	return 0;
}
